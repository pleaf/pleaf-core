<?php
/**
 * Created by PhpStorm.
 * User: sts
 * Date: 20/02/18
 * Time: 10:06
 */
namespace Sts\PleafCore\Auth;

use Sts\PleafCore\CoreException;
use Sts\PleafCore\DateUtil;

class PleafAuth {

    public function __constructor() {

    }

    public function attempt($credentials = []) {
       return $this->credentials($credentials);
    }

    private function credentials($payload) {
        $credentials = $this->getModel();
        $username = $payload["username"];

        $result = $credentials::whereRaw(" UPPER(username) = UPPER('$username') ")->first();
        
        if(!is_null($result)){
            $hashPassword = md5($payload["password"]);

            if($hashPassword != $result->password) {
                throw new CoreException("failed.authentication", [], ["password" => "Password not found"]);
            }

            $token =  str_replace("/",".",$this->encryptedToken($this->getSessionId(), $result->private_key));

            $result->token = $token;
            $result->session_id = $this->getSessionId();
            $this->generateFile($token, $result);

        } else {
            throw new CoreException("failed.authentication", [], ["username" => "Username not found"]);
        }

        return $token;

    }

    public function toUser($token = ""){
        $filePath = \Config::get('pleaf_auth.storage')."/".$token.".json";
        if(!file_exists($filePath)){
            throw new CoreException("token.invalid", [], ["error" => "Token Invalid"]);
        }

        return $this->readFile($filePath);

    }

    public function invalidToken($token = "") {

        $filePath = \Config::get('pleaf_auth.storage')."/".$token.".json";
        if(!file_exists($filePath)){
            throw new CoreException("token.invalid", [], ["error" => "Token Invalid"]);
        }

        unlink($filePath);
    }

    private function getSessionId() {
       return md5(\Config::get('pleaf_auth.secret').DateUtil::dateTimeNow());
    }

    private function encryptedToken($sessionId, $privateKey) {
        return base64_encode(openssl_encrypt($sessionId, $this->methodAes256(), $privateKey, OPENSSL_RAW_DATA, $this->ivAes256()));
    }

    private function methodAes256() {
        return 'aes-256-cbc';
    }

    private function ivAes256() {
        return chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
    }

    private function getModel() {
        return \Config::get('pleaf_auth.providers.model');
    }

    private function generateFile($token, $content) {
        $file = fopen(\Config::get('pleaf_auth.storage')."/".$token.".json","w");
        fwrite($file, $content);
        fclose($file);
    }

    private function readFile($filePath) {
        $file = file_get_contents($filePath, true);
        return json_decode($file);
    }

}