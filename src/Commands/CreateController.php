<?php
namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreateController extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'pleaf:create-controller {name} {location} {method?} {author?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a Controller {name/class} {location} {method}-optional {author}';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//input data
		$class = $this->argument('name');
		$location = $this->argument('location');
		$method = $this->argument('method');
		$listMethod = explode(",",$method);
		$author = $this->argument('author');

		$namespace = str_replace("-", " ", $location);
		$namespace = ucwords($namespace);
		$namespace = str_replace(" ", "", $namespace);
		$namespace = str_replace("/", " ", $namespace);
		$namespace = ucwords($namespace);
		$namespace = str_replace(" ", "\\", $namespace);
		\Log::debug($namespace);


		if (!is_dir(self::base_path("/packages/" . $location))) {
			$this->error('Directory not found!');
		} else {
			$this->info('Yey Bisa');


			if(count($listMethod)==0){
				$view = view("pleaf-core::templates/create-controller",
					[
						"name" => $class,
						"namespace" => $namespace,
						"listMethod" => $listMethod,
					]);
			}else{

				$view = view("pleaf-core::templates/create-controller",
					[
						"name" => $class,
						"namespace" => $namespace,
						"listMethod" => $listMethod,
					]);
			}



			// Set the path
			$path = "packages/$location/$class.php";
			$generate = false;

			if (file_exists($path)) {
				$this->error('File already exists');
				if ($this->confirm('Overwrite? [y|N]')) {
					$generate = true;
				}
			} else {
				$generate = true;
			}

			// Generate the file
			if ($generate) {
				$this->generateFile($path, $view->render());
				$this->info("Controller has successfully generated");
				$this->info("File: $path");
			}
		}
	}



	private function generateFile($path, $content)
	{
		$f = fopen($path, "w");
		fwrite($f, "<?php\n\n");
		fwrite($f, $content);
		fclose($f);
	}

	private static function base_path($path)
	{
		return base_path($path);
	}
}

