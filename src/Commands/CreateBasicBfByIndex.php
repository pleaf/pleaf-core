<?php
namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use camelCase;

class CreateBasicBfByIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pleaf:create-basic-bf-index {location_bo} {location_model} {unique_index} {author?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Basic Business Function By Index {location business object} {location model}
                               {index,index1,index2,index3,etc }{author}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $location_bo = $this->argument('location_bo');
        $location_model = $this->argument('location_model');
        $unique_index = $this->argument('unique_index');

        $namespaceBO = str_replace("-", " ", $location_bo);
        $namespaceBO = ucwords($namespaceBO);
        $namespaceBO = str_replace(" ", "", $namespaceBO);
        $namespaceBO = explode("/", $namespaceBO);
        $namespaceBO = array_splice($namespaceBO, 0, 2);
        $namespaceBO = implode("/", $namespaceBO);
        $namespaceBO = str_replace("/", " ", $namespaceBO);
        $namespaceBO = ucwords($namespaceBO);
        $namespaceBO = str_replace(" ", "\\", $namespaceBO);
        \Log::debug($namespaceBO);

        $use = str_replace("-", " ", $location_model);
        $use = ucwords($use);
        $use = str_replace(" ", "", $use);
        $use = str_replace("/", " ", $use);
        $use = ucwords($use);
        $use = str_replace(" ", "\\", $use);
//        \Log::debug($use);


        $directory = explode("/", $location_model);
        $directory = array_splice($directory, 0, 3);
        $directory = implode("/", $directory);
//        \Log::debug($directory);
        $author = $this->argument('author');
        $model = str_replace("/", " ", $location_model);
        $model = explode(" ", $model);
        $model = end($model);

        $location = str_replace("-", " ", $location_model);
        $location = ucwords($location);
        $location = str_replace(" ", "", $location);
        $location = str_replace("/", " ", $location);
        $location = ucwords($location);
        $location = str_replace(" ", "\\", $location);
        $location = "\\" . $location;
        \Log::debug($location);

        $objModel = new $location();
        $table = $objModel->getTable();
        $table = explode("_", $table);
        $table = array_splice($table, 1);
        $table = implode("_", $table);
        \Log::debug($table);
//        \Log::debug($primaryKey );


        if (!is_dir(self::base_path("/packages/" . $location_bo))) {
            $this->error('Directory not found!');
        } else {
            if (!is_dir(self::base_path("/packages/" . $directory))) {
                $this->error('Directory not found!');
            } else {
                $this->info('yaayyyyyy bisaaaaa benar');
                $basicBf = [
                    "Find".$model."ByIndex",
                    "Is".$model."ByIndex",
                    "Val".$model."ByIndex"
                ];
                $bfType = "Index";

                $strReplace = preg_replace('/\s+/S', "", $unique_index);
                $param = str_replace(",",".\", \". $","$strReplace");

                $paramKeyValue = $this->generateParamKeyValue($unique_index, "index");

                $condition = $this->generateCondition($unique_index, "index");


                foreach ($basicBf as $name) {
//
                    if ($name == "Find".$model."ByIndex") {
                        $template = "pleaf-core::templates/create-basic-bf-find";
                    } else if ($name == "Is".$model."ByIndex") {
                        $template = "pleaf-core::templates/create-basic-bf-is";
                    } else if ($name == "Val".$model."ByIndex") {
                        $template = "pleaf-core::templates/create-basic-bf-val";
                    }

//
                    // Rendering view
                    $view = view($template,
                        [
                            "name" => $name,
                            "author" => $author,
                            "date" => date('l, j F Y h:i:s A'),
                            "namespace" => $namespaceBO,
                            "paramKeyValue" => $paramKeyValue,
                            "varOutput" => $table,
                            "varOutputIndex" => lcfirst($model),
                            "locationModel" => $use,
                            "model" => $model,
                            "condition" => $condition,
                            "modelForMessage" => ucfirst(str_replace('_', ' ', snake_case($model))),
                            "bfType" => $bfType,
                            "param" => '$' . $param
                        ]);

//                     Set the path
                    $path = "packages/$location_bo/$name.php";
                    $generate = false;

                    if (file_exists($path)) {
                        $this->error('File already exists');
                        if ($this->confirm('Overwrite? [y|N]')) {
                            $generate = true;
                        }
                    } else {
                        $generate = true;
                    }

//                     Generate the file
                    if ($generate) {
                        $this->generateFile($path, $view->render());
                        $this->info("Basic Business function has successfully generated");
                        $this->info("File: $path");
                    }
                }

            }
        }


        // Save last input


    }

    private function generateParamKeyValue($paramKeyValue, $type)
    {

        $result = "";

        if (strtolower($type) == "id") {
            $result = '$' . $paramKeyValue . ' = $dto["' . $paramKeyValue . '"]; ';
        } else if (strtolower($type) == "index") {

            $index = preg_replace('/\s+/S', "", $paramKeyValue);
            $arrayIndex = explode(",", $index);

            foreach ($arrayIndex as $value) {
                $result = $result . "$" . $value . ' = $dto["' . $value . "\"]; \n\t\t";
            }

        }

        return $result;
    }

    private function generateCondition($paramKeyValue, $type)
    {

        $result = "";

        if (strtolower($type) == "id") {
            $result = 'where("' . $paramKeyValue . '", $' . $paramKeyValue . ')->';
        } else if (strtolower($type) == "index") {

            $index = preg_replace('/\s+/S', "", $paramKeyValue);
            $arrayIndex = explode(",", $index);

            foreach ($arrayIndex as $value) {
                $result = $result . 'where("' . $value . '", $' . $value . ')->';
            }

        }

        return $result;
    }

    private function generateFile($path, $content)
    {
        $f = fopen($path, "w");
        fwrite($f, "<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

    private static function base_path($path)
    {
        return base_path($path);
    }

}
