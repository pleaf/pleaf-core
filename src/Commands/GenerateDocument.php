<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 12/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Sts\PleafCommon\BO\GetTenantList;
use \zpt\anno\Annotations;
use \zpt\anno\AnnotationFactory;
use ReflectionClass;

class GenerateDocument extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pleaf:generate-doc {location}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Generate Doc BF/BT.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $location = $this->argument('location');
       $namespace = str_replace("-", " ", $location);
       $namespace = ucwords($namespace);
       $namespace = str_replace(" ", "", $namespace);
       $namespace = explode("/", $namespace);
       $namespace= array_splice($namespace, 0, 2);
       $namespace = implode("/", $namespace);
       $namespace = str_replace("/", " ", $namespace);
       $namespace = ucwords($namespace);
       $namespace = str_replace(" ", "\\", $namespace."\\"."BO");
       $locationDoc = explode("/",$location);
       $locationDoc = array_splice($locationDoc,0,2);
       $locationDoc = implode("/",$locationDoc);

       $locationDoc = self::base_path("packages/".$locationDoc);
       $folderConcat = "$locationDoc/Docs";

       if (!file_exists($folderConcat)) {
           mkdir($folderConcat);
       }

      $file_directory = $this->recursiveScan("packages/$location", $namespace, $locationDoc,$location);

      $filename2 = [];
      foreach($file_directory as $file){
            $filename = basename($file, '.php');
            $filename2[] = $filename;
        }

        foreach($file_directory as $file){
            $filename = basename($file, '.php');
            $class = $namespace . "\\" . $filename;
            $pathToFile = $locationDoc . "/Docs/" . $filename . '.html';
            $annotations = new Annotations(new ReflectionClass(new $class()));

            $data = count($annotations["in"]);
            $infoInList = [];
            if ($data > 1) {
                foreach ($annotations["in"] as $key => $value) {
                    $infoIn = (array)json_decode($value);
                    $infoInList[] = $infoIn;
                }
            } else {
                $this->info($class.' Tidak ada anotation In');;
            }

            $dataOut = count($annotations["out"]);
            $infoOutList = [];
            if($dataOut > 1 ){
                foreach( $annotations["out"] as $key=>$value) {
                    $infoOut = (array)json_decode($value);
                    $infoOutList[] = $infoOut;
                }
            } else {
                $this->info($class.' Tidak ada anotation Out');
            }


            $view = view("pleaf-core::templates/generate-doc",
                [
                    "nameBfBt" => $filename,
                    "annotationsIn" => $infoInList,
                    "annotationsOut"=>$infoOutList,
                    "filename" => $filename2
                ]);

            $this->generateFile($pathToFile, $view->render());
        }

        $outputFolder = self::base_path("packages/".$location);
        $fileIndex = substr($outputFolder, 0, -16) . "/index.html";
        $filenameBfBt[] = $filename;
        $index = view("pleaf-core::templates/index",
            [
                "filename" => $filenameBfBt
            ]);
        $this->generateFile($fileIndex, $index->render());

    }

    public function recursiveScan($dir, $namespace, $locationDoc,$location) {
        $tree = glob(rtrim($dir, '/') . '/*');
        if (is_array($tree)) {
            $file2 = [];
            foreach($tree as $file) {
                if (is_dir($file)) {
                    $file2 = array_merge($this->recursiveScan($file, $namespace, $locationDoc,$location), $file2);
                    return $file2;
                } elseif (is_file($file)) {

                    $file2 = array_merge($file2, [$file]);

                }
            }
            return $file2;

        }
    }

    private function generateFile($path,$content){
        $f = fopen($path, "w");
        fwrite($f, $content);
        fclose($f);
    }

    private static function base_path($path) {
        return base_path($path);
    }

}