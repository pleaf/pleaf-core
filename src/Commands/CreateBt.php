<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 06/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreateBt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pleaf:create-bt {name} {location} {author?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Business Transaction {nameBT} {location BT} {author}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $location = $this->argument('location');

        $namespaceBO = str_replace("-", " ", $location);
        $namespaceBO = ucwords($namespaceBO);
        $namespaceBO = str_replace(" ", "", $namespaceBO);
        $namespaceBO = explode("/", $namespaceBO);
        $namespaceBO = array_splice($namespaceBO, 0, 2);
        $namespaceBO = implode("/", $namespaceBO);
        $namespaceBO = str_replace("/", " ", $namespaceBO);
        $namespaceBO = ucwords($namespaceBO);
        $namespaceBO = str_replace(" ", "\\", $namespaceBO);
        \Log::debug($namespaceBO);

//         Save last input

        if (!is_dir(self::base_path("/packages/" . $location))) {
            $this->error('Directory not found!');
        } else {
            if (file_exists(self::base_path("/packages/" . $location . "/" . $name . ".php"))) {
                $this->error('File Exist!');
            } else {

                $view = view("pleaf-core::templates/create-bt", [
                    "namespace" => $namespaceBO,
                    "name" => $name
                ]);

                $path = "packages/$location/$name.php";
                $generate = false;

                if (file_exists($path)) {
                    $this->error("File already exists.");
                    if ($this->confirm("Overwrite? [y|N]")) {
                        $generate = true;

                    }
                } else {
                    $generate = true;
                }

                if($generate){
                    $this->generateFile($path, $view->render());
                    $this->info("Business Transaction has successfully generated");
                    $this->info("File: $path");
                }

            }
        }
    }


    private function generateFile($path, $content){
        $f = fopen($path, "w");
        fwrite($f,"<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

    private static function base_path($path) {
        return base_path($path);
    }

}
