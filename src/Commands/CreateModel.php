<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 06/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;

class CreateModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pleaf:create-model {table} {package} {author?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $table = $this->argument('table');
        $package = $this->argument('package');
        //namespace
        $namespace = str_replace("-"," ",$package);
        $namespace = ucwords($namespace);
        $namespace = str_replace(" ","",$namespace);
        $namespace = str_replace("/"," ",$namespace);
        $namespace = ucwords($namespace);
        $namespace = str_replace(" ","\\",$namespace);
        \Log::debug($namespace);


        $author = $this->argument('author');
        $name = str_replace("_"," ",$table);
        $name = ucwords($name);
        $name = explode(" ",$name);
        $name = array_slice($name,1);
        $name = implode($name);
//        \Log::debug($name);
        $location = $package;


        if(!is_dir(self::base_path("/packages/".$package))){
            $this->error('Directory not found!');
        }else {
            $select = DB::select("SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
                        FROM   pg_index i
                        JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                    AND a.attnum = ANY(i.indkey)
                        WHERE  i.indrelid ='" . $table . "'::regclass
                                    AND    i.indisprimary");
            $primary = [];
            foreach ($select as $value) {
                $primary = $value->attname;
            }
            $pk = $primary;

            \Log::debug($primary);


            $this->info("Bisa - Bisa");

            $path = self::base_path("packages/$location/$name.php");
            \Log::debug($path);
            $generate = false;

            $view = view("pleaf-core::templates/create-model", [
                "name" => $name,
                "author" => $author,
                "namespace" => $namespace,
                "primary" => $pk,
                "table" => $table,
                "location" => $location
            ]);

            if (file_exists($path)) {
                $this->error("File already exists.");
                if ($this->confirm("Overwrite? [y|N]")) {
                    $generate = true;

                }
            } else {
                $generate = true;
            }

            if ($generate) {
                $this->generateFile($path, $view->render());
                $this->info("Business Model has successfully generated");
                $this->info("File: $path");
            }
        }

    }

    private function generateFile($path, $content){
        $f = fopen($path, "w");
        fwrite($f,"<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

    private static function base_path($path) {
        return base_path($path);
    }
}
