<?php

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pleaf:reset-password {username} {new_password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to reset your password';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $pass = $this->argument('new_password');

        $env=[
            "bcrypt"=>env("ENCRYPT_TYPE","BCRYPT")
        ];


        $user = DB::table('t_user')
            ->where('username',$username)
            ->get();
        $user_id=[];
        foreach($user as $value){
            $user_id = $value->user_id;
        }

        if($user_id == NULL){
            $this->error('Username does not exists!');
        }else{
            if($env["bcrypt"]=="BCRYPT"){
                DB::table('t_user')->where('user_id', $user_id)
                    ->update([
                        "password" => Hash::make($pass)
                    ]);
            }else{
                DB::table('t_user')->where('user_id', $user_id)
                    ->update([
                        "password" => md5($pass)
                    ]);
            }
            $this->info('Password has been changed :)!');
        }
    }
}

