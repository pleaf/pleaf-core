namespace {{ $namespace }}\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use {{$locationModel}};
use DB;
use Log;

/**
 * @author {{ $author }}, {{ $date }}
 * @in
 *
 * @out
 */
class {{ $name }} extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "write description here";
    }

    public function process($dto){



        {!! $paramKeyValue !!}


        ${{ $varOutput }}  = {{ $model }}::{!! $condition !!}first();
        $output_dto["exists"] = false;
        if (!is_null(${{ $varOutput }})) {
            $output_dto["exists"] = true;
            $output_dto["{{$varOutput}}_dto"]= ${{ $varOutput }};
        }

        return $output_dto;
    }

    protected function rules() {

        return [];

    }

}