namespace {{ $namespace }}\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use {{$locationModel}};
use DB;
use Log;

/**
 * @author {{ $author }}, {{ $date }}
 * @in
 *
 * @out
 */
class {{ $name }} extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "write description here";
    }

    public function process($dto){

        {!! $paramKeyValue !!}
    	 
   	    ${{ $varOutputIndex }}  = {{ $model }}::{!! $condition !!}first();
    	
        if (is_null(${{ $varOutputIndex }})) {
            $errorList = [
               "error_{{ $varOutput }}" => "{{ $modelForMessage }} does not exists with {{ $bfType }} ".{!! $param !!}
            ];
            $this->errorBusinessValidation($errorList);
        }

        return ${{ $varOutputIndex }};

    }

    protected function rules(){

        return  [];

    }
}