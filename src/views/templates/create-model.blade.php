namespace {{ $namespace }};

use Illuminate\Database\Eloquent\Model;

/**
* @author {{ $author }}
* @in
*
* @out
*/

class {{ $name }} extends Model
{
   const TABLE_NAME      = "{{$table}}";
   protected $table      = "{{$table}}";
   protected $primaryKey = "{{$primary}}";
   public $timestamps    = false;


}
