namespace {{ $namespace }}\Providers;

use Illuminate\Support\ServiceProvider;

class {{ucwords($name_class)}}Provider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
    */
    public function boot()
    {
        //
        require __DIR__ . '/../routes.php';

        // Load views
        $this->loadViewsFrom(__DIR__.'/../views', '{{ $name_package }}');

        // Publish assets
        $this->publishes([
        __DIR__.'/../assets' => public_path('sts/{{ $name_package }}')
        ], '{{ $name_package }}');

        // Publish config
        $this->publishes([
        __DIR__.'/../config/{{$name_config}}_config.php' => config_path('{{$name_config}}_config.php'),
        ], '{{$name_config}}_config.php');

        // Publish to public folder
        $this->publishes([
        __DIR__.'/../public' => public_path(),
        ], '{{ $name_package }}-public');
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {

    }
    private function registerService($serviceName, $className) {
        $this->app->singleton($serviceName, function() use ($className) {
            return new $className;
        });
    }
}
